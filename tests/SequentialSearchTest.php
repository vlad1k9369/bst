<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Vostelmakh\Bst\Search\SequentialSearch;

class SequentialSearchTest extends TestCase
{
    public function testSearchWithIndex(): void {
        $searchValue = 'Abee';
        $field = 'name';

        $documents = [
            ["name" => "Aachen", "id" => "1"],
            ["name" => "Aarhus", "id" => "2"],
            ["id" => "3"],
            ["name" => "Abee", "id" => "4"]
        ];

        $result = (new SequentialSearch($documents, $field))->search($searchValue);

        $this->assertCount(1, $result->documents());
        $this->assertEquals($searchValue, ($result->documents()[0][$field]));
        $this->assertEquals(4, $result->comparisonCount());
    }
}
