<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Vostelmakh\Bst\BinarySearchTree\BinarySearchTreeFactory;
use Vostelmakh\Bst\IndexFile;
use Vostelmakh\Bst\Search\IndexSearch;

class IndexSearchTest extends TestCase
{
    public function testSearchWithIndex(): void {
        $searchValue = 'Aarhus';
        $field = 'name';

        $documents = [
            ["name" => "Aachen", "id" => "1"],
            ["name" => "Aarhus", "id" => "2"],
            ["id" => "3"],
            ["name" => "Abee", "id" => "4"],
            ["name" => "Aarhus", "id" => "5"],
        ];

        $factory = new BinarySearchTreeFactory($documents, new IndexFile());
        $index = $factory->create($field);

        var_dump($index);

        $result = (new IndexSearch($index))->search($searchValue);

        $this->assertCount(2, $result->documents());
        $this->assertEquals($searchValue, ($result->documents()[0][$field]));
        $this->assertEquals(5, $result->comparisonCount());
    }
}
