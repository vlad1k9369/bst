# Search Script

This script allows you to search through a set of documents using a binary search tree (BST) index. It provides both indexed and sequential search methods. The script is designed to be run via the command-line interface (CLI).

## Installation

1. Clone the repository or download the script files.
2. Install the required dependencies by running the following command in the script directory:

   ```
   composer install
   ```

## Usage

Run the script using the following command:

```
./find [options]
```

### Command Options

- `--i`        Search documents using the BST index.
- `-d`         Directory path to the file with the documents.
- `-f`         Search field.
- `-v`         Value to search for.

### Examples

1. Perform a sequential search:

   ```
   ./bin/find -d='/path/to/documents.json' -f=name -v=Aarhus
   ```

2. Perform a search using the BST index:

   ```
   ./bin/find --i -d='/path/to/documents.json' -f=name -v=Aarhus
   ```

3. Display the help message:

   ```
   ./find help
   ```
