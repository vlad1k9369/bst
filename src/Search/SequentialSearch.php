<?php

declare(strict_types=1);

namespace Vostelmakh\Bst\Search;

class SequentialSearch implements SearchInterface
{
    public function __construct(
        private readonly array $documents,
        private readonly string $field
    ) {
    }

    public function search(mixed $value): SearchResult
    {
        $result = new SearchResult();

        foreach ($this->documents as $document) {
            if (isset($document[$this->field]) && $document[$this->field] === $value) {
                $result->addDocument($document);
            }

            $result->increaseComparisonCount();
        }

        return $result;
    }
}
