<?php

declare(strict_types=1);

namespace Vostelmakh\Bst\Search;

use Vostelmakh\Bst\BinarySearchTree\BinarySearchTree;
use Vostelmakh\Bst\BinarySearchTree\Node;

class IndexSearch implements SearchInterface
{
    public function __construct(
        private readonly BinarySearchTree $index
    ) {
    }

    public function search(mixed $value): SearchResult
    {
        return $this->searchInSubTree(
            $value,
            $this->index->root,
            new SearchResult()
        );
    }

    private function searchInSubTree(
        mixed $value,
        ?Node $node,
        SearchResult $result
    ): SearchResult {
        $result->increaseComparisonCount();
        if ($node === null) {
            return $result;
        }

        $result->increaseComparisonCount();
        if ($value === $node->value()) {
            $result->setDocuments($node->documents());
            return $result;
        }

        $result->increaseComparisonCount();
        if ($value < $node->value()) {
            return $this->searchInSubTree($value, $node->left, $result);
        }

        return $this->searchInSubTree($value, $node->right, $result);
    }
}
