<?php

namespace Vostelmakh\Bst\Search;

interface SearchInterface
{
    public function search(mixed $value): SearchResult;
}
