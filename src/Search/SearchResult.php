<?php

declare(strict_types=1);

namespace Vostelmakh\Bst\Search;

class SearchResult
{
    /**
     * @param int $comparisonCount
     * @param array $documents
     */
    public function __construct(
        private int $comparisonCount = 0,
        private array $documents = []
    ) {
    }

    /**
     * @return int
     */
    public function comparisonCount(): int
    {
        return $this->comparisonCount;
    }

    /**
     * @return array
     */
    public function documents(): array
    {
        return $this->documents;
    }

    /**
     * @param array $documents
     */
    public function setDocuments(array $documents): void
    {
        $this->documents = $documents;
    }

    public function addDocument(array $document): void
    {
        $this->documents[] = $document;
    }

    public function increaseComparisonCount(): void
    {
        $this->comparisonCount++;
    }
}
