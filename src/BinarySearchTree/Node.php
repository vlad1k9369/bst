<?php

declare(strict_types=1);

namespace Vostelmakh\Bst\BinarySearchTree;

class Node
{
    private mixed $value;
    private array $documents;

    public ?Node $left = null;
    public ?Node $right = null;

    public function __construct(mixed $value, array $document)
    {
        $this->value = $value;
        $this->documents[] = $document;
    }

    /**
     * @return mixed
     */
    public function value(): mixed
    {
        return $this->value;
    }

    /**
     * @return mixed
     */
    public function documents(): mixed
    {
        return $this->documents;
    }

    /**
     * @param array $value
     *
     * @return void
     */
    public function addDocuments(array ...$documents): void
    {
        foreach ($documents as $document) {
            $this->documents[] = $document;
        }
    }
}
