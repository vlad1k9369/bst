<?php

declare(strict_types=1);

namespace Vostelmakh\Bst\BinarySearchTree;

use Vostelmakh\Bst\IndexFile;

class BinarySearchTreeFactory
{
    public function __construct(
        private array $documents,
        private IndexFile $fileManager
    ) {
    }

    public function create(string $field): BinarySearchTree
    {
        $bst = new BinarySearchTree($field);

        $cachedFilePath = $this->fileManager->getCachedFilePath($bst);
        if (file_exists($cachedFilePath)) {
            return $this->fileManager->loadIndexFromFile($cachedFilePath);
        }

        foreach ($this->documents as $document) {
            if (isset($document[$field])) {
                $bst->insert($document[$field], $document);
            }
        }

        $this->fileManager->saveIndexToFile($bst);

        return $bst;
    }
}
