<?php

declare(strict_types=1);

namespace Vostelmakh\Bst\BinarySearchTree;

class BinarySearchTree
{
    public ?Node $root = null;

    public function __construct(
        private readonly string $field
    ) {
    }

    public function insert($value, array $document): void
    {
        $this->insertNode(new Node($value, $document), $this->root);
    }

    private function insertNode(Node $node, ?Node &$subtree): void
    {
        if ($subtree === null) {
            $subtree = $node;
            return;
        }

        if ($node->value() === $subtree->value()) {
            $subtree->addDocuments(...$node->documents());
            return;
        }

        if ($node->value() > $subtree->value()) {
            $this->insertNode($node, $subtree->right);
        } else {
            $this->insertNode($node, $subtree->left);
        }
    }

    public function field(): string
    {
        return $this->field;
    }
}
