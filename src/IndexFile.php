<?php

declare(strict_types=1);

namespace Vostelmakh\Bst;

use Vostelmakh\Bst\BinarySearchTree\BinarySearchTree;

class IndexFile
{
    private const CACHED_INDEX_FILENAME_TEMPLATE = "/index_%s.dat";
    private const CACHE_DIR = __DIR__ . '/cache';

    public function saveIndexToFile(BinarySearchTree $index): bool
    {
        $serializedIndex = serialize($index);
        return file_put_contents($this->getCachedFilePath($index), $serializedIndex);
    }

    public function loadIndexFromFile(string $filePath): ?BinarySearchTree
    {
        $serializedIndex = file_get_contents($filePath);
        if ($serializedIndex === false) {
            return null;
        }

        return unserialize($serializedIndex, [BinarySearchTree::class]);
    }

    public function getCachedFilePath(BinarySearchTree $index): string
    {
        $filename = sprintf(self::CACHED_INDEX_FILENAME_TEMPLATE, $index->field());
        return self::CACHE_DIR . $filename;
    }
}
